package com.example.calculator

import android.annotation.SuppressLint
import android.content.Context
import com.example.calculator.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class CalculatorApplication : DaggerApplication() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var contextInstance: Context
    }

    override fun onCreate() {
        super.onCreate()

        contextInstance = applicationContext
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}