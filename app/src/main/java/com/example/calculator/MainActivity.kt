package com.example.calculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.calculator.features.calculator.CalculatorFragment

class MainActivity : AppCompatActivity() {

    companion object {
        private const val FRAGMENT_CALCULATOR = "TAG_CALCULATOR"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CalculatorFragment(), FRAGMENT_CALCULATOR)
                .commit()
        }
    }
}
