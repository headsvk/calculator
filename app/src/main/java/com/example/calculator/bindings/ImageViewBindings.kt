package com.example.calculator.bindings

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

object ImageViewBindings {

    @BindingAdapter("imageUrl", "placeholder", requireAll = false)
    @JvmStatic
    fun loadImage(imageView: ImageView, imageUrl: String?, placeholder: Drawable?) {
        Glide.with(imageView)
            .load(imageUrl)
            .apply(RequestOptions().placeholder(placeholder))
            .into(imageView)
    }
}
