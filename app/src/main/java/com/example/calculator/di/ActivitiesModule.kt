package com.example.calculator.di

import com.example.calculator.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Provide all your activities here.
 */
@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

}
