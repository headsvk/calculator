package com.example.calculator.di

import com.example.calculator.CalculatorApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivitiesModule::class, FragmentsModule::class, ComponentModule::class])
interface AppComponent : AndroidInjector<CalculatorApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: CalculatorApplication): Builder

        fun build(): AppComponent
    }
}
