package com.example.calculator.di

import androidx.lifecycle.ViewModelProvider
import com.example.calculator.FlavorManager
import com.example.data.api.ApiService
import com.example.data.components.ExpenseComponent
import com.example.domain.adapters.BigDecimalAdapter
import com.example.domain.components.IExpenseComponent
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module(subcomponents = [(ViewModelSubComponent::class)])
class AppModule {

    @Provides
    @Singleton
    internal fun provideMoshi(): Moshi {
        return Moshi.Builder()
                .add(BigDecimalAdapter)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideSolarService(moshi: Moshi): ApiService {

        return Retrofit.Builder()
                .baseUrl(FlavorManager.apiConfig.baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create(ApiService::class.java)
    }

    @Singleton
    @Provides
    internal fun provideViewModelFactory(viewModelSubComponent: ViewModelSubComponent.Builder): ViewModelProvider.Factory {
        return ViewModelFactory(viewModelSubComponent.build())
    }

}
