package com.example.calculator.di

import com.example.data.api.ApiService
import com.example.data.components.ExpenseComponent
import com.example.domain.components.IExpenseComponent
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ComponentModule {

    @Provides
    @Singleton
    internal fun provideExpenseComponent(apiService: ApiService): IExpenseComponent {
        return ExpenseComponent(apiService)
    }
}
