package com.example.calculator.di

import com.example.calculator.features.calculator.CalculatorFragment
import com.example.calculator.features.expense.ExpenseDialogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    internal abstract fun contributeCalculatorFragment(): CalculatorFragment

    @ContributesAndroidInjector
    internal abstract fun contributeExpenseDialogFragment(): ExpenseDialogFragment

}
