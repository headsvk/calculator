package com.example.calculator.di

import androidx.collection.ArrayMap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.calculator.features.calculator.CalculatorViewModel
import com.example.calculator.features.expense.ExpenseViewModel
import java.util.concurrent.Callable
import javax.inject.Singleton

@Singleton
class ViewModelFactory constructor(viewModelSubComponent: ViewModelSubComponent) : ViewModelProvider.Factory {
    private val creators: ArrayMap<Class<out ViewModel>, Callable<out ViewModel>> = ArrayMap()

    init {
        creators[CalculatorViewModel::class.java] = Callable<ViewModel> { viewModelSubComponent.mainActivity() }
        creators[ExpenseViewModel::class.java] = Callable<ViewModel> { viewModelSubComponent.expenseDialog() }
    }


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator: Callable<out ViewModel>? = creators[modelClass]
        if (creator == null) {
            for ((key, value) in creators) {
                if (modelClass.isAssignableFrom(key)) {
                    creator = value
                    break
                }
            }
        }
        if (creator == null) {
            throw IllegalArgumentException("Unknown model class $modelClass")
        }
        try {
            return creator.call() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}
