package com.example.calculator.di

import com.example.calculator.features.calculator.CalculatorViewModel
import com.example.calculator.features.expense.ExpenseViewModel
import dagger.Subcomponent

@Subcomponent
interface ViewModelSubComponent {

    fun mainActivity(): CalculatorViewModel
    fun expenseDialog(): ExpenseViewModel

    @Subcomponent.Builder
    interface Builder {
        fun build(): ViewModelSubComponent
    }
}
