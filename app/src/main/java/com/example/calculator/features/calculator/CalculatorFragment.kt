package com.example.calculator.features.calculator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.calculator.databinding.FragmentCalculatorBinding
import com.example.calculator.features.expense.ExpenseDialogFragment
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class CalculatorFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(CalculatorViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return FragmentCalculatorBinding.inflate(inflater, container, false)
            .also { it.viewModel = viewModel }
            .root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.expenses.observe(this, Observer { expense ->
            ExpenseDialogFragment.newInstance(expense).show(requireFragmentManager(), TAG_DIALOG)
        })
    }

    companion object {
        private const val TAG_DIALOG = "dialog"
    }
}
