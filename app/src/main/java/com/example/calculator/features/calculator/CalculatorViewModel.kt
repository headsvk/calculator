package com.example.calculator.features.calculator

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.calculator.common.SingleLiveEvent
import com.example.domain.components.CalculatorComponent
import com.example.domain.components.IExpenseComponent
import com.example.domain.entities.Expense
import com.example.domain.entities.InputValue
import com.example.domain.entities.RegisteredExpense
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.math.BigDecimal
import javax.inject.Inject

class CalculatorViewModel
@Inject constructor(private val expenseComponent: IExpenseComponent) : ViewModel(), InputValueListener {

    val expenses = SingleLiveEvent<RegisteredExpense>()

    val inputText = ObservableField("")
    private val calculator = CalculatorComponent()

    private val subscriptions = CompositeDisposable()

    override fun onInputValueClicked(inputValue: InputValue) {
        with(calculator) {
            inputText.set(inputText.get()!! add inputValue)
        }
    }

    fun onAddExpense(view: View) {
        val expense = calculator.parseInput(inputText.get()!!)
        if (expense > 0) {
            expenseComponent.registerExpense(Expense("TODO", BigDecimal.valueOf(expense)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onSuccess = {
                    expenses.value = it
                }, onError = {
                    Log.e(CalculatorViewModel::class.java.simpleName, "Failed to register expense", it)
                })
                .addTo(subscriptions)
        }
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }
}
