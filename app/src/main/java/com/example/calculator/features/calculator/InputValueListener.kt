package com.example.calculator.features.calculator

import com.example.domain.entities.InputValue

interface InputValueListener {
    fun onInputValueClicked(inputValue: InputValue)
}
