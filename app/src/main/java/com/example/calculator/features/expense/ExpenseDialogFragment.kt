package com.example.calculator.features.expense

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.calculator.R
import com.example.calculator.databinding.FragmentExpenseBinding
import com.example.domain.entities.RegisteredExpense
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject

class ExpenseDialogFragment : DaggerAppCompatDialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ExpenseViewModel::class.java)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), R.style.Theme_Design_BottomSheetDialog)
        val binding = FragmentExpenseBinding.inflate(LayoutInflater.from(requireContext()))
        binding.viewModel = viewModel
        viewModel.initialize(arguments!!.getParcelable(ARG_EXPENSE))
        dialog.setContentView(binding.root)
        return dialog
    }

    companion object {
        private const val ARG_EXPENSE = "expense"

        fun newInstance(expense: RegisteredExpense) = ExpenseDialogFragment().apply {
            arguments = Bundle().apply { putParcelable(ARG_EXPENSE, expense) }
        }
    }
}
