package com.example.calculator.features.expense

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.calculator.FlavorManager
import com.example.domain.entities.RegisteredExpense
import java.text.NumberFormat
import javax.inject.Inject

class ExpenseViewModel
@Inject constructor() : ViewModel() {

    val imageUrl = FlavorManager.apiConfig.logoUrl
    val amount = ObservableField<String>()
    val cashback = ObservableField<String>()

    fun initialize(expense: RegisteredExpense) {
        val formatter = NumberFormat.getCurrencyInstance()
        amount.set(formatter.format(expense.amount))
        cashback.set(formatter.format(expense.cashback))
    }
}
