package com.example.calculator

import com.example.domain.config.IApiConfig
import com.example.mastercard.MastercardApiConfig

object FlavorManager {

    val apiConfig: IApiConfig
        get() = MastercardApiConfig
}
