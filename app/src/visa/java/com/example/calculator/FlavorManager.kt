package com.example.calculator

import com.example.domain.config.IApiConfig
import com.example.visa.VisaApiConfig

object FlavorManager {

    val apiConfig: IApiConfig
        get() = VisaApiConfig
}
