package com.example.data.api

import com.example.domain.entities.Expense
import com.example.domain.entities.RegisteredExpense
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {

    @POST("expenses")
    fun registerExpense(@Body expense: Expense): Single<RegisteredExpense>
}
