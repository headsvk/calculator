package com.example.data.components

import com.example.data.api.ApiService
import com.example.domain.components.IExpenseComponent
import com.example.domain.entities.Expense
import com.example.domain.entities.RegisteredExpense
import io.reactivex.Single

class ExpenseComponent(private val apiService: ApiService) : IExpenseComponent {

    override fun registerExpense(expense: Expense): Single<RegisteredExpense> {
        return apiService.registerExpense(expense)
    }
}