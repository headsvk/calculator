package com.example.domain.components

import com.example.domain.entities.InputAction
import com.example.domain.entities.InputNumber
import com.example.domain.entities.InputOperator
import com.example.domain.entities.InputValue
import java.text.NumberFormat

/**
 * Calculator logic
 */
class CalculatorComponent {

    private var lastOperator = InputOperator.OPERATOR_PLUS
    private var lastValue = 0.0

    val numberFormat: NumberFormat = NumberFormat.getNumberInstance().apply {
        maximumFractionDigits = 8
    }

    infix fun String.add(inputValue: InputValue): String {

        return when (inputValue) {
            is InputNumber -> {
                val newValue = this + inputValue.displayName
                when {
                    // don't format number if ends with decimal point
                    inputValue == InputNumber.NUMBER_POINT -> newValue
                    // don't format decimal number if ends with zero
                    inputValue == InputNumber.NUMBER_0 && newValue.contains(InputNumber.NUMBER_POINT.displayName) -> newValue
                    else -> numberFormat.format(parseInput(newValue))
                }
            }
            is InputOperator -> {
                val text = this add InputAction.ACTION_EQ
                lastOperator = inputValue
                lastValue = parseInput(text)
                ""
            }
            is InputAction -> {
                when (inputValue) {
                    InputAction.ACTION_CLEAR -> {
                        if (this.isNotEmpty()) {
                            this.dropLast(1)
                        } else this
                    }
                    InputAction.ACTION_CLEAR_ACC -> {
                        clearState()
                        ""
                    }
                    InputAction.ACTION_EQ -> {
                        val currentValue = parseInput(this)
                        val newValue = lastOperator.op(lastValue, currentValue)
                        val text = numberFormat.format(newValue)
                        clearState()
                        text
                    }
                }
            }
            else -> throw IllegalArgumentException("Unknown InputValue")
        }
    }

    private fun clearState() {
        lastOperator = InputOperator.OPERATOR_PLUS
        lastValue = 0.0
    }

    fun parseInput(input: String): Double {
        val text = input.removeSuffix(InputNumber.NUMBER_POINT.displayName)
        return if (text.isNotEmpty()) {
            return numberFormat.parse(text).toDouble()
        } else 0.0
    }
}