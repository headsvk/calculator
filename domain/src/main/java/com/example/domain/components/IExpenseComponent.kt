package com.example.domain.components

import com.example.domain.entities.Expense
import com.example.domain.entities.RegisteredExpense
import io.reactivex.Single

interface IExpenseComponent {
    fun registerExpense(expense: Expense): Single<RegisteredExpense>
}
