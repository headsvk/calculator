package com.example.domain.config

interface IApiConfig {
    val baseUrl: String
    val logoUrl: String
}