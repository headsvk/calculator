package com.example.domain.entities

import java.math.BigDecimal

data class Expense(
    val name: String,
    val amount: BigDecimal
)
