package com.example.domain.entities

import java.text.DecimalFormatSymbols

interface InputValue {
    val displayName: String
}

enum class InputNumber(override val displayName: String) : InputValue {

    NUMBER_0("0"),
    NUMBER_1("1"),
    NUMBER_2("2"),
    NUMBER_3("3"),
    NUMBER_4("4"),
    NUMBER_5("5"),
    NUMBER_6("6"),
    NUMBER_7("7"),
    NUMBER_8("8"),
    NUMBER_9("9"),
    NUMBER_POINT(DecimalFormatSymbols.getInstance().decimalSeparator.toString())
}

enum class InputOperator(val op: (Double, Double) -> Double, override val displayName: String) : InputValue {

    OPERATOR_PLUS(Double::plus, "+"),
    OPERATOR_MINUS(Double::minus, "-"),
    OPERATOR_MUL(Double::times, "*"),
    OPERATOR_DIV(Double::div, "/");
}

enum class InputAction(override val displayName: String) : InputValue {
    ACTION_EQ("="),
    ACTION_CLEAR("C"),
    ACTION_CLEAR_ACC("AC")
}
