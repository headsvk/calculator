package com.example.domain.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class RegisteredExpense(
    val name: String,
    val amount: BigDecimal,
    val cashback: BigDecimal
): Parcelable
