package com.example.domain

import com.example.domain.components.CalculatorComponent
import com.example.domain.entities.InputAction.*
import com.example.domain.entities.InputNumber.*
import com.example.domain.entities.InputOperator.*
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Unit tests for [CalculatorComponent].
 *
 * Local unit test, which will execute on the development machine (host).
 */
class CalculatorComponentUnitTest {

    @Test
    fun numberInt_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_1 add NUMBER_3 add NUMBER_5 add NUMBER_5 add NUMBER_0
            assertEquals(numberFormat.format(13550), result)
        }
    }

    @Test
    fun numberReal_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_2 add NUMBER_3 add NUMBER_POINT add NUMBER_1 add NUMBER_1 add NUMBER_0 add NUMBER_5
            assertEquals(numberFormat.format(23.1105), result)
        }
    }

    @Test
    fun additionInt_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_2 add OPERATOR_PLUS add NUMBER_3 add OPERATOR_PLUS add NUMBER_5 add ACTION_EQ
            assertEquals(numberFormat.format(10), result)
        }
    }

    @Test
    fun additionReal_isCorrect() {
        with(CalculatorComponent()) {
            val result =
                "" add NUMBER_1 add NUMBER_POINT add NUMBER_4 add
                        OPERATOR_PLUS add NUMBER_3 add NUMBER_POINT add NUMBER_5 add ACTION_EQ
            assertEquals(numberFormat.format(4.9), result)
        }
    }

    @Test
    fun subtractionInt_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_9 add OPERATOR_MINUS add NUMBER_3 add OPERATOR_MINUS add NUMBER_2 add ACTION_EQ
            assertEquals(numberFormat.format(4), result)
        }
    }

    @Test
    fun subtractionReal_isCorrect() {
        with(CalculatorComponent()) {
            val result =
                "" add NUMBER_6 add NUMBER_POINT add NUMBER_3 add
                        OPERATOR_MINUS add NUMBER_9 add NUMBER_POINT add NUMBER_6 add ACTION_EQ
            assertEquals(numberFormat.format(-3.3), result)
        }
    }

    @Test
    fun multiplicationInt_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_9 add OPERATOR_MUL add NUMBER_5 add OPERATOR_MUL add NUMBER_2 add ACTION_EQ
            assertEquals(numberFormat.format(90), result)
        }
    }

    @Test
    fun multiplicationReal_isCorrect() {
        with(CalculatorComponent()) {
            val result =
                "" add NUMBER_5 add NUMBER_POINT add NUMBER_5 add
                        OPERATOR_MUL add NUMBER_9 add NUMBER_POINT add NUMBER_5 add ACTION_EQ
            assertEquals(numberFormat.format(52.25), result)
        }
    }

    @Test
    fun divisionInt_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_8 add OPERATOR_DIV add NUMBER_2 add OPERATOR_DIV add NUMBER_1 add ACTION_EQ
            assertEquals(numberFormat.format(4), result)
        }
    }

    @Test
    fun divisionReal_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_1 add NUMBER_0 add NUMBER_POINT add NUMBER_5 add
                    OPERATOR_DIV add NUMBER_3 add NUMBER_POINT add NUMBER_0 add ACTION_EQ
            assertEquals(numberFormat.format(3.5), result)
        }
    }

    @Test
    fun clear_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_1 add NUMBER_0 add NUMBER_POINT add NUMBER_5 add ACTION_CLEAR add
                    ACTION_CLEAR add OPERATOR_DIV add NUMBER_2 add ACTION_EQ
            assertEquals(numberFormat.format(5), result)
        }
    }

    @Test
    fun clearAccumulator_isCorrect() {
        with(CalculatorComponent()) {
            val result = "" add NUMBER_1 add NUMBER_0 add OPERATOR_MUL add NUMBER_5 add ACTION_CLEAR_ACC add
                    NUMBER_1 add OPERATOR_PLUS add NUMBER_2 add ACTION_EQ
            assertEquals(numberFormat.format(3), result)
        }
    }
}