package com.example.mastercard

import com.example.domain.config.IApiConfig

object MastercardApiConfig : IApiConfig {

    override val baseUrl: String
        get() = "https://thawing-cove-94670.herokuapp.com/api/mastercard/"
    override val logoUrl: String
        get() = "https://thawing-cove-94670.herokuapp.com/mastercard.png"
}
