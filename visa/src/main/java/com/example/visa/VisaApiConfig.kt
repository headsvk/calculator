package com.example.visa

import com.example.domain.config.IApiConfig

object VisaApiConfig : IApiConfig {

    override val baseUrl: String
        get() = "https://thawing-cove-94670.herokuapp.com/api/visa/"
    override val logoUrl: String
        get() = "https://thawing-cove-94670.herokuapp.com/visa.png"
}
